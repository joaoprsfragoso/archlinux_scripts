# update the system clock
timedatectl set-ntp true

# partition the disks
parted -s /dev/sdb mktable gpt
parted -s /dev/sdb mkpart primary fat32 1MiB 1024MiB
parted -s /dev/sdb set 1 boot on
parted -s /dev/sdb mkpart primary 1025MiB 100%
# parted -s /dev/sda mktable gpt
# parted -s /dev/sda mkpart primary 1MiB 90%
# parted -s /dev/sda mkpart primary 90% 100%

# format the partitions
mkfs.vfat /dev/sdb1
mkfs.xfs /dev/sdb2 
# mkfs.xfs /dev/sda1
# mkswap -L SWAP /dev/sda2
swapon /dev/sda2

# mount the file systems
mount /dev/sdb2 /mnt
mkdir /mnt/boot /mnt/home
mount /dev/sdb1
mount /dev/sda1

# select the mirrors
pacman -Sy pacman-contrib
curl -s 'https://www.archlinux.org/mirrorlist/?country=FR&country=GB&protocol=https&use_mirror_status=on' | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 -

# install the base packages
pacstrap /mnt base base-devel linux intel-ucode

# fstab
genfstab -U /mnt >> /mnt/etc/fstab

# chroot
arch-chroot /mnt

# time zone
ln -sf /usr/share/zoneinfo/Europe/Lisbon /etc/localtime
hwclock --systohc

# localization
sed -i -e 's/#en_US\.UTF-8 UTF-8/en_US\.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# network configuration
echo 'LAPTOP' > /etc/hostname
echo '127.0.0.1		localhost' > /etc/hosts
echo '::1			localhost' >> /etc/hosts
echo '127.0.1.1 	LAPTOP.localdomain	LAPTOP' >> /etc/hosts

# root password
passwd

# boot loader
bootctl --path=/boot install
echo 'default       arch' > /boot/loader/loader.conf
echo 'editor        no' >> /boot/loader/loader.conf
echo 'auto-entries  no' >> /boot/loader/loader.conf
echo 'auto-firmware no' >> /boot/loader/loader.conf
echo 'console-mode  max' >> /boot/loader/loader.conf
echo 'title     Arch Linux' > /boot/loader/entries/arch.conf
echo 'linux     /vmlinuz-linux' >> /boot/loader/entries/arch.conf
echo 'initrd    /intel-ucode.img' >> /boot/loader/entries/arch.conf
echo 'initrd    /initramfs-linux.img' >> /boot/loader/entries/arch.conf
echo 'options   root=/dev/sdb2 quiet' >> /boot/loader/entries/arch.conf

# echo "MANUAL STEP NEEDED"
# echo "MODULES=(intel_agp i915)"
# echo "HOOKS=(base systemd ... block sd-lvm2 filesystems)"
# echo "mkinitcpio -p linux"

# mkinitcpio -p linux