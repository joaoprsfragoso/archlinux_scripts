pacman -Syyuu --noconfirm
pacman -S linux-headers
pacman -S dkms
pacman -S bbswitch-dkms
pacman -S sudo

echo "blacklist nouveau" >> /etc/modprobe.d/blacklist
echo "blacklist rivafb" >> /etc/modprobe.d/blacklist
echo "blacklist nvidiafb" >> /etc/modprobe.d/blacklist
echo "blacklist rivatv" >> /etc/modprobe.d/blacklist
echo "blacklist nv" >> /etc/modprobe.d/blacklist
echo "blacklist uvcvideo" >> /etc/modprobe.d/blacklist.conf

echo "options bbswitch load_state=0 unload_state=1" > /etc/modprobe.d/load.conf
echo "bbswitch" > /etc/modules-load.d/bbswitch.conf

useradd --create-home johnny
passwd johnny

#pvcreate /dev/sdb
#vgextend vg_root /dev/sdb
#lvcreate --type cache-pool -l 100%FREE -n lv_cachepool vg_root /dev/sdb
#lvconvert --type cache --cachepool /dev/vg_root/lv_cachepool /dev/vg_root/lv_root
#lvs -a -o +devices

echo "MANUAL STEP NEEDED"
echo "EDITOR=vim visudo"
echo "gpasswd -a jack wheel"

LD_LIBRARY_PATH="~/.steam/steam/ubuntu12_32/steam-runtime/amd64/usr/lib/x86_64-linux-gnu/" ~/.steam/bin32/steam-runtime/run.sh ~/.local/share/Steam/steamapps/common/Football\ Manager\ 2018\ Demo/fm

sudo lvs -a -o +devices
