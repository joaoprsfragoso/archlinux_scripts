# secure erase all disks
# hdparm -I /dev/sda | grep frozen
# hdparm --user-master u --security-set-pass PasSWorD /dev/sda
# hdparm --user-master u --security-erase PasSWorD /dev/sda

hdparm -I /dev/sdb | grep frozen
hdparm --user-master u --security-set-pass PasSWorD /dev/sdb
hdparm --user-master u --security-erase PasSWorD /dev/sdb
